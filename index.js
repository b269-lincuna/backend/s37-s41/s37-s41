
//Booking System MVP requirements

//Course Booking App

/*
	* User Registration
	* User authentication (login)
	* Retrieval of authenticated user's details
	* Course creation by an authenticated user
	* Retrieval of courses/ (user-active , admin-active/inactive)
	* Course info updateing by an authenticated user
	* Course archiving by an authenticated user
*/

/* BOOKING SYSTEM API Dependencies
Booking System API - Business Use Case Translation to Model Design
	* express
	* mongoose
	* bcrpt - for hashing user passwords prior to storage	
	* cors - for allowing cross-origin resource sharing (preparation
			 for full-stack dev)
	* jsonwebtoken - for implementing JSON web tokens
*/

// CODE ALONG - DISCUSSION S37

//Setup dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors"); // (preparation for full-stack dev)

// This allows us to use all the routes defined in "userRoute.js"
const userRoute = require("./routes/userRoute");
const courseRoute = require("./routes/courseRoute");

// Server Setup
const app = express();

//Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// DATABASE CONNECTION [START] *****************
mongoose.connect("mongodb+srv://jamepaullincuna:admin123@zuitt-bootcamp.k9jjqkm.mongodb.net/courseBookingAPI?retryWrites=true&w=majority",
	{useNewUrlParser: true,useUnifiedTopology: true}
);

mongoose.connection.once("open", () => console.log("Now connected to the cloud database!"));
// DATABASE CONNECTION [ END ] *****************

// allows all the user routes created in "userRoute.js" file to use "/users" as route (resources
//locathost:4000/users/
app.use("/users", userRoute);
app.use("/courses", courseRoute);

//Server listening
//process.env.PORT will used the defined port number for the application whenever environment variable is available or used port 4000 if none is defined
// This syntax will allow flexibility when the application locally or as a hosted application
app.listen(process.env.PORT || 4000 , () => console.log(`Now listening to port ${process.env.PORT || 4000}.`));
