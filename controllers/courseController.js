

const Course = require("../models/Course");


/*module.exports.addCourse = (reqBody) => {
	let newCourse = new Course ({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});
	return newCourse.save().then((course, error ) => !error)
};*/

//******************* ACTIVITY S39 [START] **********************
/*module.exports.addCourse = (reqBody, userData) => {
	// console.log(userData);
	if (userData.isAdmin) {
		let newCourse = new Course ({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		});
		
		return newCourse.save().then((course, error ) => !error)
	} else {
		console.log(`You are not allowed to add Course!`);
		return false;
	}
};*/
//******************* ACTIVITY S39 [ END ] **********************

// ACTIVITY S39 BEST PRACTICE [START] ***************************

module.exports.addCourse = (data) => {
	if (data.isAdmin) {
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});
		return newCourse.save().then((course, error) => {
			// Course creation successful
			if (error) {
				return false;
			} else {
				return true; // Course creation failed
			};
		});
	}

	let message = Promise.resolve("User must be ADMIN to access this!");
	return message.then((value)=>{
		return {value};
	});
};

// ACTIVITY S39 BEST PRACTICE [ END ] ***************************


// DISCUSSION S40 [START] ***************************************
//retrieve ALL Courses
module.exports.getAllCourses=()=>{
	return Course.find({}).then(result => {
		return result;
	});
};

// Retrieve active courses
module.exports.getActiveCourses=()=>{
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

// Retrieve specific courses
module.exports.getCourse=(reqParams)=>{
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

// Update courses
module.exports.updateCourse=(reqParams, reqBody)=>{
	let updateCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse)
	.then((course , error) => {
		if (error){	return false;}
		else{return true;}
	});
};

// DISCUSSION S40 [ END ] ********************************************


//********************** ACTIVITY S40 [START] ************************
// Patch Archive course
module.exports.archiveCourse=(reqBody, reqParams)=>{
	let updateCourse = {isActive : reqBody.isActive};
	return Course.findByIdAndUpdate( reqParams.courseId , updateCourse ).then((course,error) => !error);
};

//********************** ACTIVITY S40 [ END ] ************************

//*************** ACTIVITY S40 Best Practive [START] *****************
module.exports.archiveCourse2=(reqParams)=>{
	let updateCourse = {isActive : false};
	return Course.findByIdAndUpdate( reqParams.courseId , updateCourse ).then((course,error) => !error);
};
//*************** ACTIVITY S40 Best Practive [ END ] *****************





