
// Check if email already exists
/*
Business Logic:
1. Use mongoose "find" method to find duplicate emails
2. Use the "then" method to send a response back to the
3. Postman application based on the result of the "find" method
*/

const User = require("../models/User");
const Course = require("../models/Course");

// "bcrypt" is a password-hashing function that is commonly used in computer systems to store user password securely
const bcrypt = require("bcrypt");

const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then( result => (result.length > 0));
};

/*module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0){
			return true;
		}else {
			return false;
		};
	});
};*/

// CODE ALONG - DISCUSSION S38 [START] **********

/*
BUSINESS LOGIC
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

// "bcrypt" is a password-hashing function that is commonly used in computer systems to store user password securely
//const bcrypt = require("bcrypt");
//bcrypt.hashSync(variable,10)  10 - number of round in hashing

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		// "bcrypt.hashSync" is function in the bcrypt library that used to generate hash value for a given input string synchonously
		// "reqBody.password" - input string that need to hash
		// 10 - user defined value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt password
		password : bcrypt.hashSync(reqBody.password,10)
	});

	//return newUser.save().then((user, error ) => !error)

	return newUser.save().then((user, error ) => {
		if (error){return false;} 
		else {return true;}
	})
};


/*
	Business Logic:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result=>{
		if (result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		};
	});
};

// CODE ALONG - DISCUSSION S38 [ END ] **********

// ************** S38 ACTIVITY [START] ***********
	/* 	Business Logic:
		1. Find the document in the database using the user's ID
		2. Reassign the password of the returned document to an empty string
		3. Return the result back to the frontend/Postman
	*/

	module.exports.getProfile = (data) => {
		return User.findById(data.id).then(result => {
			result.password = "";
			return result;
		});
	};

// ************** S38 ACTIVITY [ END ] ***********



// S41 DISCUSSION [START] *****************************

//Authenticated User Enrollment In Active Course

/*Workflow:
1. User logs in, server will respond with JWT on successful auth 
2. A POST request with the JWT in its header and the course ID in its request body will be sent to the /user/enroll endpoint
3. Server validates JWT
	a. if valid , user will be enrolled in the course
	b. if invalid, deny request
*/

// S41 DISCUSSION [ END ] *****************************


// S41 ACTIVITY [START] *****************************
// Authenticate user enrollment

/*  Business Logic:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database */

module.exports.enroll = async (data) => {
	// Add courseId in the enrollments array of the user
	let isUserUpdated = await User.findById(data.userId).then(user=>{
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, error)=> {
			if(error){return false;}
			else{return true;}
		});
	});

	// Add user Id in the enrollees array of the course
	let isCourseUpdated = await Course.findById(data.courseId).then(course=>{
		course.enrollees.push({userId: data.userId});
		return course.save().then((user, error)=> {
			if(error){return false;}
			else{return true;}
		});
	});

	if (isUserUpdated && isCourseUpdated){
		return true;
	}else {
		return false;
	}
};
// S41 ACTIVITY [START] *****************************

