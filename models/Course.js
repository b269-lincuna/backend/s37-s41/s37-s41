// ************** S37 ACTIVITY [START] ***********
/*
const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name : { type : String , required : [true, "Name is required"] },
	description : { type : String, required : [true,"Description is required"]},
	price : { type : Number, required : [true, "Price is required"] },
	isActive : { type : Boolean, required : true },
	isAdmin : {	 type : Boolean, required : true },
	createdOn : { type : Date, required : [true, "Date is required"] },
	enrollees : [{
		userId : { type : String, required : [true, "User ID is required"] },	
		enrolledOn : {  type : Date, required: true, default : new Date() }
	}]
});

module.exports = mongoose.model("Course",courseSchema);
*/

// ************** S38 ACTIVITY [ END ] ***********

const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name : { type : String,
		required : [true, "Course is required"]
	},
	description : {	type : String,
		required : [true, "Description is required"]
	},
	price : { type : Number,
		required : [true, "Price is required"]
	},
	isActive : { type : Boolean,
		default : true
	},
	createdOn : { type : Date,
		default : new Date()
	},
	enrollees : [
		{
			userId : { type : String,
				required: [true, "UserId is required"]
			},
			enrolledOn : { type : Date,
				default : new Date() 
			}
		}
	]
});

module.exports = mongoose.model("Course", courseSchema);
