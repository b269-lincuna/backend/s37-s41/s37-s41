
// CODE ALONG - DISCUSSION S38 [START] **********


// JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
const jwt = require("jsonwebtoken");

/* JWT ANATOMY

	Header - type of token and signing algorithm
	Payload - contains the claims (id, email, isAdmin)
	Signature - generate by putting the encoded header, the encoded payload and applying the algorithm in the header

*/

// Used in the algorithm for encrypting our data which makes it difficult to decode the information without defined secret keyword
const secret = "CourseBookingAPI";

// Token Creation
module.exports.createAccessToken = (user) => {
	// payload
	const data ={
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	// "jwt.sign" method use to generate a JSON web token
	return jwt.sign(data, secret, {}); // {} can be use the session duration

};


// CODE ALONG - DISCUSSION S38 [ END ] **********



// CODE ALONG - DISCUSSION S39 [START] **********
/* 
BOOKING SYSTEM API
Authorization via JWT, Retrieval of user details
*/

// Token Verification
module.exports.verify=(req,res,next)=>{
	// console.log("seq1");
	let token= req.headers.authorization;
	if(typeof token !=="undefined"){
		// console.log("seq2");
		//console.log(token);
		// use slice method because req.headers.authorization has 'bearer '
		token= token.slice(7,token.length);
		return jwt.verify(token, secret, (err,data) => {
			if (err){
				// console.log("seq3");
				return res.send({auth:"failed"}); 
				// could be {"auth":"failed"}
				// or could be {auth:"failed"}
			} else {
				// console.log("seq4");
				next(); // runs the next function below this function code
			}
		})
	}else {
		return res.send({auth:"failed"});
	}
};

// Token decryption
module.exports.decode = (token) => { // token source from verify function
	if (typeof token !== "undefined"){
		// console.log("seq5");
		token = token.slice(7,token.length);
		return jwt.verify(token, secret, (err,data) => {
			if (err){
				// console.log("seq6");
				return null;
			} else {
				// console.log("seq7");
				return jwt.decode(token,{complete: true}).payload;
			}
		})
	} else {
		return null;
	}
};


// CODE ALONG - DISCUSSION S39 [END] **********