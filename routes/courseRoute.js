const express = require("express");
const router = express.Router();
const auth = require("../auth");
const courseController = require("../controllers/courseController");

/*
router.post("/details", auth.verify, (req,res) => {
	// console.log("seqdet");
	const userData = auth.decode(req.headers.authorization);//decode from token
	userController.getProfile({id:userData.id}).then(result => res.send(result));
});
*/

/*router.post("/addCourse", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	courseController.addCourse(req.body).then(result => res.send(result));
});*/


//********************** ACTIVITY S39 [START] **************************
/*router.post("/addCourse", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	courseController.addCourse(req.body, userData).then(result => res.send(result));
});*/
//********************** ACTIVITY S39 [ END ] **************************


//********************** ACTIVITY S39 BEST PRACTICE [START] ************
router.post("/create", auth.verify, (req,res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(result => res.send(result));
});
//********************** ACTIVITY S39 BEST PRACTICE [ END ] ************

//********************** DISCUSSION S40 [START] ************************
router.get("/all",(req,res)=>{
	courseController.getAllCourses().then(result => res.send(result));
});

router.get("/active",(req,res)=>{
	courseController.getActiveCourses().then(result => res.send(result));
});

router.get("/:courseId",(req,res)=>{
	courseController.getCourse(req.params).then(result => res.send(result));
});

router.put("/:courseId",(req,res)=>{
	courseController.updateCourse(req.params, req.body).then(result => res.send(result));
});
//********************** DISCUSSION S40 [ END ] ************************


//********************** ACTIVITY S40 [START] **************************
router.patch("/:courseId", auth.verify, (req,res) => {
	courseController.archiveCourse(req.body,req.params).then(result => res.send(result));
});
//********************** ACTIVITY S40 [ END ] **************************

//***************** ACTIVITY S40 Best Practice [START] *****************
router.patch("/:courseId/archive", auth.verify, (req,res)=>{
  courseController.archiveCourse2(req.params).then(result=>res.send(result));
});
//***************** ACTIVITY S40 Best Practice [START] *****************
module.exports = router;

