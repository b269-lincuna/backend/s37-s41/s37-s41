const express = require("express");

// Creates a router instance that functions as middleware and routing system
const router = express.Router();

const userController = require("../controllers/userController");
const auth = require("../auth");

// Route to get all the task
// http://localhost:3001/tasks
/*router.get("/", (req,res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});*/

//route for checking email
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
});

//route for user registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(result => res.send(result));
});

// Route for user authentication
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(result => res.send(result));
});

// Route for getting user profile
// router.post("/details", (req,res) => {
// 	userController.getProfile(req.body).then(result => res.send(result));
// });

router.post("/details", auth.verify, (req,res) => {
	// console.log("seqdet");
	const userData = auth.decode(req.headers.authorization);//decode from token
	userController.getProfile({id:userData.id}).then(result => res.send(result));
});


// S41 ACTIVITY [START] *****************************

router.post("/enroll", auth.verify, (req,res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(result => res.send(result));
});

// S41 ACTIVITY [ END ] *****************************


module.exports = router;

